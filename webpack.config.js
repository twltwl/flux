module.exports = {
  entry: "./app/src/main.js",
  devtool: 'source-map',
  cache: true,
  output: {
    path: __dirname,
    filename: "bundle.js"
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel'
    }]
  },
};
