import Dispatcher from '../dispatcher';
import constants from '../constants';

let listActions = {
  add(element) {
    Dispatcher.dispatch({
      actionType: constants.ADD,
      element: element
    });
  },
  toggle(element) {
    Dispatcher.dispatch({
      actionType: constants.TOGGLE,
      element: element
    });
  },
  loadInitial() {
    Xhr.get('/mock/list.json')
    .then(function (value) {
        Dispatcher.dispatch({
          actionType: constants.LOAD_INITIAL,
          data: JSON.parse(value)
        });
    }, function (reason) {
        console.log(reason);
    });
  }
};

export {listActions as default};
