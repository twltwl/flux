import Dispatcher from '../dispatcher';
import constants from '../constants';
import EventEmitter from 'events';
import R from 'ramda';

let list = {};

let ListState = R.merge(EventEmitter.prototype, {

  getState() {
    return list;
  },

  dispatcherIndex: Dispatcher.register(action => {
    switch (action.actionType) {
      case constants.TOGGLE:
        if(!list[action.element]){
          list[action.element] = true;
        } else {
          list[action.element] = !list[action.element];
        }
        console.log(list);
        ListState.emit('change');
      break;
    }
    return true;
  })

});


export {ListState as default};
