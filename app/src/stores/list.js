import Dispatcher from '../dispatcher';
import constants from '../constants';
import EventEmitter from 'events';
import R from 'ramda';
import Xhr from '../util/xhr';

let list = [];

let List = R.merge(EventEmitter.prototype, {

  getStuff() {
    return list;
  },
  
  dispatcherIndex: Dispatcher.register(action => {
    switch (action.actionType) {
      case constants.ADD:
        list.push(action.element);
        List.emit('change');
      break;
      case constants.LOAD_INITIAL:
        list = action.data;
        List.emit('change');
      break;
    }
    return true;
  })

});


export {List as default};
