import React from 'react';
import Maincomponent from './components/main.comp';

window.r = React.createElement;

if (document.readyState === 'complete' || document.readyState === 'loaded') {
  drawApp();
} else {
  document.addEventListener('DOMContentLoaded', drawApp);
}

function drawApp() {
  document.removeEventListener('DOMContentLoaded', drawApp);
  React.render(
    r(Maincomponent),
    document.getElementById('app')
  );
}
