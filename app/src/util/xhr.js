class Xhr {
  
  constructor(){}
  
  get(url) {
    return new Promise(
      function (resolve, reject) {
          let request = new XMLHttpRequest();
          request.open('GET', url);
          request.send();
          request.onreadystatechange = function () {
              if(request.readyState === 4){
                if (request.status === 200) {
                    // Success
                    resolve(request.response);
                } else {
                    // Something went wrong (404 etc.)
                    reject(new Error(request.statusText));
                }
              }
          }
          request.onerror = function () {
              reject(new Error(
                  'XMLHttpRequest Error: '+request.statusText));
          };

      });
  }
}

export default new Xhr();