const consts = {
  ADD           : 'add',
  TOGGLE        : 'toggle',
  LOAD_INITLIAL : 'load_initial'
};

export {consts as default};
