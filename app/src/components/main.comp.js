import React from 'react';
import List from '../stores/list';
import ListActions from '../actions/listactions';
import ListItem from './item';

let getListState = () => {
  return {
    list  : List.getStuff(),
  };
};

export default React.createClass({

  getInitialState () {
    return getListState();
  },

  componentDidMount () {
    List.addListener('change', this.onChange);
  },
  
  componentWillMount () {
    ListActions.loadInitial();
  },

  componentWillUnmount () {
    List.removeListener('change', this.onChange);
  },

  onChange () {
    this.setState(getListState());
  },

  render () {
    let list = this.state.list;
    
    return r('div', {}, [
      r('div', {},
        list.map(val => r(ListItem, {data:val}))
      ),
      r('button', {onClick: () => {
        ListActions.add(Math.max(...list) + 1);
      }},'add')
    ]);
  }
});
