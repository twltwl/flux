import React from 'react';
import ListState from '../stores/list-state';
import listActions from '../actions/listactions';

let getState = () => {
  return {
    state : ListState.getState()
  };
}

export default React.createClass({

  getInitialState () {
    return getState();
  },

  componentDidMount () {
    ListState.addListener('change', this.onChange);
  },

  componentWillUnmount () {
    ListState.removeListener('change', this.onChange);
  },

  onChange () {
    this.setState(getState());
  },

  render () {
    let val = this.props.data;
    let state = this.state.state;
    console.log(val);
    return r('a',{onClick: () => {
          listActions.toggle(val);
        }, 'className' : (state[val] ? 'active' : '')},val);
  }
});
